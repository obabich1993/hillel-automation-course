package Hometask6;

import java.util.Scanner;

public class Array {

    public static void main(String[] args){
        //Task 1
        int[] newArray = new int[10];
        int[] secondArray = new int[10];


        for (int i = 0; i < 10; i++) {
            newArray [i] = (int)(Math.random()*11);
            secondArray [i] = (int)(Math.random()*11);
        }

        double aver1 = 0;
        double aver2 = 0;

        for (int i = 0; i < 10; i++) {
            aver1 += newArray[i];
            aver2 += secondArray[i];
        }
        aver1/=5;
        aver2/=5;

        if(aver1 > aver1){
            System.out.println("Среднее арифметическое первого массива ("+aver1+") больше среднего арифметического "+
                    "второго массива ("+aver2+")");
        } else if(aver1 < aver2){
            System.out.println("Среднее арифметическое первого массива ("+aver1+") меньше среднего арифметического "+
                    "второго массива ("+aver2+")");
        } else {
            System.out.println("Средние арифметические массивов равны ("+aver1+")");
        }
        //Task3
        int[] thirdArray = new int[8];
        thirdArray[0] = 13;
        thirdArray[1] = 10;
        thirdArray[2] = 1;
        thirdArray[3] = 83;
        thirdArray[4] = 3;
        thirdArray[5] = 5;
        thirdArray[6] = 9;
        thirdArray[7] = 2;
        System.out.println("Большее число из массива: " + thirdArray[3]);
        System.out.println("Меньшее число из массива: " + thirdArray[2]);

        //Task 4
        int[] fourthArray = new int[10];
        fourthArray[0] = 1;
        fourthArray[1] = 2;
        fourthArray[2] = 3;
        fourthArray[3] = 4;
        fourthArray[4] = 5;
        fourthArray[5] = 6;
        fourthArray[6] = 7;
        fourthArray[7] = 8;
        fourthArray[8] = 9;
        fourthArray[9] = 10;
        int length = 0;
        for (int element : fourthArray)
        {
            length++;
        }
        for (int i = length - 1; i >= 0; i--)
        {
            System.out.println(fourthArray[i]);
        }

    }
}




