package hometask5;

public class Loops {
    public static void loops() {
        //Hometask 5
//1)	Необходимо вывести cпоследовательность: 3 9 15 21 27 33 39 45 51 57 63 69 (сделать через do/while и for)
        //FOR
        int[] numbers = {3, 9, 15, 21, 27, 33, 39, 45, 51, 57, 63, 69};
        for (int x : numbers) {
            System.out.print(x);
            System.out.print("\n");
        }

//DO/WHILE
        int number = 3;
        int result = number;
        int i = 1;
        do {
            System.out.println(result);
            i++;
            result = result + 6;
        } while (result < 70);

            /*3)	Вы заходите в атб и у вас 200 грн.
- вы набираете товар на 202 грн, и тогда выводе “не хватает 2 grn”,
- вы набираете продуктов на 200 грн и выводе счастливчик,
- набираете продуктов на 100 грн  и выводе – пойду возьму еще что то
-вводите другое число и выводите что то пошло не так.
Использовать оператор Switch,  должно выводиться на экран только 1 сообщение, если нет совпадений, то сообщение по дефолту.
*/
        int money = 3000;
        switch (money) {
            case 202:
                System.out.println("не хватает 2 grn");
                break;
            case 200:
                System.out.println("счастливчик");
                break;
            case 100:
                System.out.println("пойду возьму еще что то");
                break;
            case 500:
                System.out.println("что то пошло не так");
                break;

            default:
                System.out.println("some message");

        }
            /*A Универы выставляют оценки по такой шкале
a. Ниже 25 - F
b. 25 to 45 - E
c. 45 to 50 - D
d. 50 to 60 - C
e. 60 to 80 - B
f. Выше 80 - A
*/
        int grade = 100;
        switch (grade) {
            case 25:
                System.out.println("F");
                break;
            case 45:
                System.out.println("E");
                break;
            case 50:
                System.out.println("D");
                break;
            case 60:
                System.out.println("C");
                break;
            case 80:
                System.out.println("B");
                break;
            case 100:
                System.out.println("A");
                break;
                default:
                System.out.println("some grade");

        }

    }

}
