package codingbat;


public class WarmupOne {
    /*Given two int values, return their sum.
Unless the two values are the same, then return double their sum.*/

    public static boolean sleepIn(boolean weekday, boolean vacation) {
        if (weekday == false || vacation == true) {
            return true;
        }
        return false;
    }

    /*We have two monkeys, a and b, and the parameters aSmile and bSmile indicate if each is smiling.
We are in trouble if they are both smiling or if neither of them is smiling.
Return true if we are in trouble.*/
    public boolean monkeyTrouble(boolean aSmile, boolean bSmile) {
        if ((aSmile && bSmile) || (!aSmile & !bSmile)) {
            return true;
        }
        return false;
    }
/*
Given two int values, return
their sum. Unless the two values are the same, then return double their sum.*/

    public int sumDouble(int a, int b) {
        int x = 0;
        if (a == b) {
            return x = (a + b) * 2;
        }
        x = a + b;
        return x;
    }

    /*Given an int n, return the
    absolute difference between n and 21,
    except return double the absolute difference if n is over 21.
     */
    public int diff21(int n) {
        if (n > 21) {
            return Math.abs(n - 21) * 2;
        }
        return Math.abs(n - 21);
    }

    /*
We have a loud talking parrot. The "hour" parameter is the current hour time in the range 0..23.
We are in trouble if the parrot is talking and the hour is before 7 or after 20.
Return true if we are in trouble.*/
    public boolean parrotTrouble(boolean talking, int hour) {
        if ((talking == true) && (hour < 7 || hour > 20)) {
            return true;
        }
        return false;
    }

    /*
Given 2 ints, a and b, return true if one if them is 10 or if their sum is 10.*/
    public boolean makes10(int a, int b) {
        if ((a == 10 || b == 10 || a + b == 10)) {
            return true;
        }
        return false;
    }

    /* Given an int n, return true if it is within 10 of 100 or 200.
    Note: Math.abs(num) computes the absolute value of a number.
     */
    public boolean nearHundred(int n) {
        if ((n >= 90) && (n <= 110) ||
                (n >= 190) && (n <= 210)) {
            return true;
        }
        return false;
    }

    /*
    Given 2 int values, return true if one is negative and one is positive.
    Except if the parameter "negative" is true, then return true only if both are negative.*/
    public boolean posNeg(int a, int b, boolean negative) {
        if ((negative) && (a < 0 && b < 0)) {
            return true;
        } else {
            if ((negative == false) && (a > 0 && b < 0 || b > 0 && a < 0)) {
                return true;
            }
            return false;
        }
    }

    /*
   Given a string, return a new string where "not " has been added to the front.
   However, if the string already begins with "not", return the string unchanged.
   Note: use .equals() to compare 2 strings.*/
    public static String notString(String str) {
        String s = str. ;
        if (str. > 2 && str.substring(0, 3).equals("not")) {
            return s = str;
        } else {
            s = "not " + str;
        }
        return s;
    }public String notString(String str) {
        String s;
        if (str.length > 2 && str.substring(0, 3).equals("not")) {
            return s = str;
        } else {
            s = "not " + str;
        }
        return s;
    }

    /*
   Given a non-empty string and an int n, return a new string where the char at index n has been removed.
   The value of n will be a valid index of a char in the original string
   (i.e. n will be in the range 0..str.length()-1 inclusive).*/
    public String missingChar(String str, int n) {
        int size = str.length();
        return str.substring(0, n) + str.substring(n + 1);
    }


    /*
            //Given a string, return a new string where the first and last chars have been exchanged.*/
    public static String frontBack(String str) {
        if (str.length() <= 1) return str;

        char first = str.charAt(0);
        char last = str.charAt(str.length() - 1);
        String mid = str.substring(1, str.length() - 1);
        return last + mid + first;
    }

    /*Given a string, we'll say that the front is the first 3 chars of the string.
    If the string length is less than 3, the front is whatever is there.
    Return a new string which is 3 copies of the front.
     */
    public String front3(String str) {
        String front;
        if (str.length() <= 3) {
            front = str;
        } else {
            front = str.substring(0, 3);
        }
        return front + front + front;
    }

    /*
    Given a string, take the last char and return a new string with the last char
    added at the front and back, so "cat" yields "tcatt". The original string will be length 1 or more.
    */
    public String backAround(String str) {
        char last = str.charAt(str.length() - 1);
        return last + str + last;
    }

    /*
    Return true if the given non-negative number is a multiple of 3 or a multiple
     of 5. Use the % "mod" operator -- see Introduction to Mod*/
    public boolean or35(int n) {
        return n % 3 == 0 || n % 5 == 0;
    }

    /*
    Given a string, take the first 2 chars and return the string with the 2 chars added at both the front
     and back, so "kitten" yields"kikittenki". If the string length is less than 2, use whatever chars are there.*/
    public String front22(String str) {
        String front;

        if (str.length() <= 2) {
            front = str;
        } else {
            front = str.substring(0, 2);
        }

        return front + str + front;
    }

    /*Given a string, return true if the string starts with "hi" and false otherwise.*/
    public boolean startHi(String str) {

        return str.startsWith("hi");
    }
    public String extraEnd(String str)
    {
        int len = str.length();
        String temp = str.substring(len-2, len);
        return (temp + temp + temp);
    }

}

